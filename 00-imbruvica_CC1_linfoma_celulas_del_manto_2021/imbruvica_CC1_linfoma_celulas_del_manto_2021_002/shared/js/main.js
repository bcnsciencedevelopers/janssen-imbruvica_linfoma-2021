$(document).ready(function () {

    menu();
    main();
    espec();

});

function main() {

    // Logo
    $("#global").append('<div id="logo1" class="btn"></div>');
    $("#global").append('<div id="logo2" class="btn"></div>');
    //menu
    $("#global").append('<div id="menu1" class="btn js-goto-touch" data-presentation="0" data-slide="1"></div>');
    
    $("#global").append('<div id="menu5" class="btn js-goto" data-presentation="0" data-slide="2"></div>');
    $("#global").append('<div id="btn-ft" class="btn js-goto" data-presentation="99" data-slide="0"></div>');
    // $("#global").append('<div id="home" class="btn js-goto" data-presentation="0" data-slide="0"></div>');
   
   
//glosario
    $("#global").append('<div id="menu2" class="btn js-goto" data-presentation="0" data-slide="8"></div>');
    
        // Bibliografia
    $("#global").append('<div id="menu4" class="btn js-goto" data-presentation="0" data-slide="7"></div>');
       
    
   
       
       
             
   
       
       
         

//flechas
    $("#global").append('<div id="fletxa-next" class="btn js-goto-next"></div>');
    $("#global").append('<div id="fletxa-prev" class="btn js-goto-prev"></div>');
}

/****************
 FUNCIONES
 ****************/

function openPop(id) {

    $("#" + id).css('display', "block");
    $("#" + id).css('z-index', 999);
    $("#lightbox").css('z-index', 1);
    $("#lightbox").animate({
        opacity: 0.9
    }, edetailing.popupActual.dataTemps, function () { });
    $("#" + id).animate({
        opacity: 1
    }, edetailing.popupActual.dataTemps, function () { });
}

function closePop(id) {

    $("#lightbox").animate({
        opacity: 0
    }, edetailing.popupActual.dataTemps, function () {
        $("#lightbox").css('z-index', -50);
    });
    $("#" + id).animate({
        opacity: 0
    }, edetailing.popupActual.dataTemps, function () {
        $("#" + id).css('display', "none");
        $("#" + id).css('z-index', -50);
    });
}